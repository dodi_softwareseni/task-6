<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpresstraining_task6');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'em1xI4ebz7.nYy8^b{+F%r]X_His*$jJ]h{a>OIv)nW4Ca!XJtZXxjjck!nR=dSR');
define('SECURE_AUTH_KEY',  ':=4:,CUY2L5}LHKwg$y~Y8ax>_A`Ccny/OO)>zu)Dj,8zdj}FqA5*Dkx=:73_*|d');
define('LOGGED_IN_KEY',    '`2e>:v?b=F*|X0Wk/pOT8~M JX=CW`7`B?Zz4EsfC{OdOChvs>&?ykgC9@l<*yc-');
define('NONCE_KEY',        'S$ZS*-1fmJc.lG=#owghKi@Mp#SsYx@}2+tx;;})J5[`QtVz)*p<ZM$zpF,-(pKE');
define('AUTH_SALT',        'Pi^]W;o_*O_UkO,-]CJS3$o0qc]I$SrgMFvTn{z0^8CmZ+s1!*eVda5pD;u0d7%#');
define('SECURE_AUTH_SALT', 'e0rS3(-;@&~K}r2|9qaaW5YPJV8{e/4>j,xx24fZ?),^;!QAihbq1EC+b`5T{.<O');
define('LOGGED_IN_SALT',   ',A8purNN9z$CsOh9k,!qz-O;o*+kwDs</trFjpcH2*rP* s2CUUt@$J6G%p,n#Fu');
define('NONCE_SALT',       '5[Wn=26FTuEffsgfmF!U~c):rB?O%5xNWXb,4T#cvuD9|<Q5*/z8brf< *HY9lr<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/wordpresstraining_task6/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
