<?php 

function wpbootstrap_scripts()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-autocomplete');
    wp_register_style( 'jquery-ui-styles','http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui-styles' );
    wp_enqueue_style( 'style', get_template_directory_uri().'/css/bootstrap.css');

    wp_enqueue_script( 'my_autocomplete',get_template_directory_uri().'/js/my_autocomplete.js',array('jquery'));
    wp_localize_script( 'my_autocomplete', 'my_autocomplete', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts' );

function ajax_shortcode(){
    // ob_start();
    ?>
        <br>
        <form action="" method="GET">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <input id="searchInput" name="searchInput" type="text" class="form-control" placeholder="Post Title Search" value=""><br>
                    </div>
                    <div class="col-lg-6">
                        <input name="btnSearch" value="Search" type="submit" class="btn btn-primary"></input>
                    </div>
                </div>
            </div>
            <div id="results"></div><br>

        </form>
        
    <?php
    // return ob_get_clean();
}

add_shortcode('ajax_sc', 'ajax_shortcode');

function my_search() {
    $searchInput=$_POST["searchInput"];
    //var_dump($_POST['searchInput']) ;

    $my_args = array(
        'post_status'  => 'publish', 
        'post_title' => $searchInput              
    );
     
    $json=array();
     
    $custom_query = new WP_Query( $my_args );
     
    if ( $custom_query->have_posts() ) {
        while ( $custom_query->have_posts() ) {
            $custom_query->the_post();
     
                $json[]=array( 
                    'label'=> get_the_title(),
                    'value'=> get_the_title() 
                );
     
              } 
    }
     
    echo json_encode($json);
    wp_die();
}
add_action( 'wp_ajax_my_search', 'my_search' );
add_action( 'wp_ajax_nopriv_my_search', 'my_search' );

add_filter( 'posts_where', 'yourr_title_func', 10, 2 );
function yourr_title_func( $where, &$the_query )
{
    global $wpdb;
    if ( $post_title = $the_query->get( 'post_title' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \''.'%' . $wpdb->esc_like( $post_title )  . '%\'';
    }
    return $where;
}


function display_search() { 
    if(isset($_GET['btnSearch'])){
        $searchInput=$_GET["searchInput"];
        $paged = max( 1, get_query_var('page') );

        $my_args = array(
            'post_status'  => 'publish', 
            'post_title' => $searchInput,   
            'posts_per_page' => 5,  // to show 5 data 
            'paged' => $paged,
        );
         
        $json=array();
         
        $custom_query = new WP_Query( $my_args);

        ?>
            <div class="wrap">
            <h2>Post Title</h2>
            <br><br>
            <table border="2">
                <tr>
                    <!-- <th>No</th> -->
                    <th>Title</th>
                </tr>
        <?php
         
        if ( $custom_query->have_posts() ) {
            // $no=1;
            while ( $custom_query->have_posts() ) {
                $custom_query->the_post();        
                ?>
                    <tr>
                        <!-- <td><?php echo $no++;?></td>  -->
                        <td><?php the_title();?></td>
                    </tr>  
                <?php
            } 

            wp_reset_postdata();
        }
        else{
            echo "No data";
        }

        ?>
        </table>
        </div>
        <?php
        echo paginate_links( 
            array(
                'current' => $paged,
                'total' => $custom_query->max_num_pages,
        ) );
        
    }
}
add_shortcode( 'fun_display_search', 'display_search' );


?>