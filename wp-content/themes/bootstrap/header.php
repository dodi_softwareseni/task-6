<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


        <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    	<?php wp_head(); ?>
    	
    </head>

    <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
    <br><br><br>
    	<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>

    </div>

		<div class="container">